oh-i-see	oh I see.
oh hi	oh hi
You learn south african sign language 	you will learn south african sign language
a	a
b	b
c	c
d	d
e	e
f	f
g	g
h	h
i	i
j	j
k	k
k	k
l	l
m here	m finger is placed here
n	n
o	o
p k-down p	p is the same as k but down p
q	q
r	r
s	s
t	t
u	u
v	v
w	w
x	x
y	y
z	z
hello	hello
hello	hello
how-are-you	how are you?
how-are-you	how are you?
name you what	what is your name?
name you what	what is your name?
you live where	where do you live?
you live where	where do you live?
you work what	what work do you do?
you work what	what work do you do?
you study what	what are you studying?
you study what	what are you studying?
university you study where	which university are you at?
university you study where	which university are you at?
student	student
student	student
what	what
who	who
why	why
which	which
when	when
how	how
how much	how much
how much	how much
how many	how many
how many	how many
how long	how long
how long	how long
old you	how old are you?
old you	how old are you
or	or
no	no
no	no
yes	yes
yes	yes
but	but
have	have
same	same
different	different
because	because
again	again
after	after
before	before
you understand	do you understand?
me misunderstand	i do not understand
please	please
thank-you	thank you
sorry	sorry
question	question
know	i know
remember	i remember
forgot	i forgot
now go yes	can i go now?
now go yes	can i go now?
bye	bye bye
nice meet-you	it was nice to meet you
nice meet-you	it was nice to meet you
deaf	deaf
hearing	hearing
learn	learn
learn	learn
like	like
like	like
love	love
mean	meaning
meet	meet
meet	meet
sign slow please	sign slow please
sign slow please	sign slow please
facilitator	facilitator
lecture	lecture
lecture person	lecturer
tutorial	tutorial
tutorial person	tutor
hello welcome	hello welcome
today interview why before you learn sign language where U-C-T	we want to interview you today because you studied sign language at UCT
how-are-you	how are you?
I-am-good thank-you you how-are-you?	I am well thank you. How are you?
i-am-good thank-you	I am good, thank you
now interview why you tell-me-about your experience	now for the interview. I would love to hear all about your experiences. 
sign language you learn how	how did you learn sign language?
i start university cape-town 20-14	i started studying at UCT in 2014
first year study what linguistics	in first year I studied linguistics
we-recieved-email say what two deaf students want have teach sign language	we recieved an email saying that there are two deaf students who want to teach sign language
i think O-K join class and learn learn with Roobyn Yuvini	I thought, ok, i'll join the class and I started to learn with Robyn and Yuvini
interesting sign language learn why	interesting. Why did you learn sign language?
when start learn why i thing new.	When I started learning it was because it was something new. I wanted to be an interpreter but not for sign language, for italian and arabic. So spoken language things. 
want interpret but sign language no want interpret for i-t-a-l-i-a-n italian a-r-a-b-i-c arabic.	I wanted to be an interpreter but not for sign language, for italian and arabic.
hearing translation things	So spoken language things. 
but when start learn sign language wow my-mind-opened.	But when i started learning sign language, wow, my mind opened.
learnt many applications for sign language.	I learnt that there are many applications for sign language.
now interpreter change help deaf community way deaf community help-me	Now I want to change my language to interpret to sign language and help the Deaf community in the same way the Deaf community helped me. 
think advice-give other hearing people learn sign language have 	do you have any advice to give to other hearing people who want to learn sign language? 
good advice hearing people start learn sign language.	good advice to give hearing people who are starting to learn sign language.
wow.	wow.
many 	many things to say
say most important need learn how recieve eyes not ears.	The most important thing to learn is to listen with your eyes and not your ears.
because sign language all visual.	Because sign language is all visual.
if recieve lip-reading will-not help-you need sign-watch how sign mean learn fast. also need conversation deaf person easy learn.  	If you try to lip read it will not help you,you need to focus on the person's hands and look how they sign and what the signs mean. You will learn faster.
also need conversation deaf person easy learn.  	Also you also need to converse with a deaf person quickly because it's easier to learn.   
sign language deprivation your thoughts what	what are your thoughts on sign language deprivation
yes big problem.	yes it is a big problem.
specifically south africa why people do-not-understand important sign language give deaf child.	yes it is a big problem, specifically in south africa because people do not understand the importance of giving a child sign language 
 the-child grows-up open-minded grammar intergrate can world perfectly 	the child grows up open minded which allows them to intergrate into the world
without deaf children growing-up grammer-will-not-understand meaning-will-not-understand isolated society because people deaf try communicate but grow-up language deprivation have-no language input. 	without language, a deaf child will not understand grammar and meaning and therefore be isolated by society- people will try to communicate but because of language deprivation there is limited language input. 
growing-up communication-lost time alone isolated	growing up with lost communication, you are alone and isolated. 
consider deaf community you apart	do you consider yourself as part of the deaf community
hmm yes	hmm, yes
i-think question complicated	I think the question is complicated.
me person hearing topic within member deaf community	 I am a hearing person which means that my being a member of the deaf community....
deaf community inside circle inside no	the inner circle of the deaf community no
but outer circle deaf community yes	but the outer circle of the deaf community yes
born deaf circle in centre close deaf community	the inner circle consists of people who are born deaf- the close deaf commmunity
but if hearing know sign language outer-circle yes	but if you are hearing and you know sign language you are part of the outer circle. 
inner-circle deaf community but topic deaf obviously	the inner circle of the deaf community are people who are deaf obviously
I think i am a member of the deaf community also because i am training to be a sign language interpreter.	for-me i-think yes-member deaf community also why interpreter change
but inner deaf no 	but the inner circle, am I deaf? no. 
you learn sign language up-until-now happen one embarrassing what 	what is your most embarrassing moment learning sign language
one time go restuarant robyn yuvini friend chris robyn dad myself	Once we went to a restuarant with robyn, yuvini, our friend chris, robyn's dad and myself 
all-five sat-at-table restuarant chris say vegetarian	All five of us sat at the table in the restuarant and chris said that he is vegetarian. 
yuvini ask vegetarian v-e-g-a-n different what 	Yuvini asked what the difference is between vegetarian and vegan is
i explain say well vegetarian can eat pets 	i explained saying that vegetarians can eat pets 
this-pets pets mean animal products	this is pets. I meant to say animal products
I see robyn drinking and then she puts her hand over her mouth because she wants to spit but she needs to swallow it.	see drinking hand-over-mouth need spit swallow
Her dad is sitting opposite and she will spit on him	because dad opposite spit on him
wow embarrassing wow	wow that was an embarrassing moment
challenges	Any challenges?
challenges wow	challenges wow
start live robyn yuvini short one two weeks 	I started living with robyn and yuvini and we were living together for one to two weeks. 
go see play about wheelchair challenges people wheelchair see all daily 	We went to go see a play about wheelchair users and the challenges they face on a faily basis. 
 music after when dancing wheelchair dancing but interpreter say oh need me interpret songs no bye walked-away	After the show there was music and dancing but the interpreter said "oh, you do not need me to interpret the songs, bye" and walked away. 
mean interpreter gone	the interpreter was gone
i sit robyn yuvini interpret songs for-myself because not-qualified interpreter	I sat with robyn and yuvini interpreting the songs for myself because I was not a qualified interpreter
tap-on-my-shoulder i-look-up deaf man say please go interpret please go interpret 	I felt a tap on my shoulder and there was a deaf man who said "please go interpret, please go interpret" 
me cannot interpret training not-start just-staying deaf people	I cannot interpret- I have not started my training to be an interpreter and I only just started living with Deaf people
please go better nothing	he said "please go, it's better than nothing" 
o-k better nothing i-walk sit stage start interpreting songs	I though- okay, it's better than nothing so walked to the stage and sat there interpreting the songs
thank god know songs before but difficult fast need sign quickly sign-to-keep-up	thank god i knew the songs before but it was still difficult because i needed to sign quickly and keep up
but for-me good challenge because i-went forced there interpret need interpret because help friends mine.	For me it was a good challenge because i was forced to interpret because I wanted to help my friends.
for-me good good challenge	For me it was a good challenge because i was forced to interpret because I wanted to help my friends. for me it was a good challenge
interesting. your experience you-signed-to-me wow	for me it was a good challenge.
I sure viewers watch enjoy	I am sure our viewers enjoyed watching.
thank you	thank you
next week focus what vocabulary first relationships second U-C-T third numbers fouth time.	next week we will focus on the following vocabulary: relationships, UCT, numbers and time.
plus excited interview who Jabaar.	Plus I am excited to interview Jabaar. 
see-you next week.	See you all next week.
I-love-you	I love you 

# ------------------------------------------------------------------------
# Author:       Jake Pencharz
# Description:  Pre-process text data from ELAN to generate a 
#               parrelel corpus of language data. Required to remove the
#               newline character between the sentences, and rather make
#               them tab delimited.
# Date:         July 2019
# Project:      Translate English to SASL, UCT Final year project
# ------------------------------------------------------------------------
import glob
import os


INPUT_DATA_DIR = "../data/ELAN_TextFiles/"
OUTPUT_DATA_DIR = "../data/eng-gloss/"
input_filename = "ELAN_SASL_Lesson1.txt"
output_filename = "eng-gloss.txt"


f_new = open(OUTPUT_DATA_DIR + output_filename, "a+")

'''
Strip the newline from the parrelel corpus and add a tab between the sentence pairs
'''
for input_filename in glob.glob(os.path.join(INPUT_DATA_DIR, '*.txt')):
    f_raw = open(input_filename, "r")
    print("Reading from file {}".format(input_filename))
    content = f_raw.readlines()
    j = 0
    for i, line in enumerate(content, start = 1):
        if i%2 == 1:
            f_new.write( line.rstrip("\n") + '\t' )
        else:
            f_new.write( line )





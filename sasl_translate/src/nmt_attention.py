# ------------------------------------------------------------------------
# Author:       Jake Pencharz
# Description:	Main Script For Training and Translation
# Date:         July 2019
# Project:      Translate English to SASL, UCT Final year project
#
# ** Based on tensorflow tutorial:
# https://www.tensorflow.org/beta/tutorials/text/nmt_with_attention
# ------------------------------------------------------------------------

# VS Code linter problem fixed using:
# https://code.visualstudio.com/docs/python/environments
# - cmd-shft-P
# - python:select interpreter

from __future__ import absolute_import, division, print_function
from model import Encoder, Decoder, loss_function
from load_data import load_dataset, preprocess_sentence
import time
import os
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import tensorflow as tf
tf.enable_eager_execution()


print("Tensorflow version {} imported".format(tf.__version__))


# ------------------------------
# TRAINING
# ------------------------------
def train():
    print("\n...Let the training begin...")

    EPOCHS = 10

    for epoch in range(EPOCHS):
        start = time.time()

        hidden = encoder.initialize_hidden_state()
        total_loss = 0

        for (batch, (inp, targ)) in enumerate(dataset):
            loss = 0

            with tf.GradientTape() as tape:
                enc_output, enc_hidden = encoder(inp, hidden)

                dec_hidden = enc_hidden

                dec_input = tf.expand_dims(
                    [targ_lang.word2idx['<start>']] * BATCH_SIZE, 1)

                # Teacher forcing - feeding the target as the next input
                for t in range(1, targ.shape[1]):
                    # passing enc_output to the decoder
                    predictions, dec_hidden, _ = decoder(
                        dec_input, dec_hidden, enc_output)

                    loss += loss_function(targ[:, t], predictions)

                    # using teacher forcing
                    dec_input = tf.expand_dims(targ[:, t], 1)

            batch_loss = (loss / int(targ.shape[1]))

            total_loss += batch_loss

            variables = encoder.variables + decoder.variables

            gradients = tape.gradient(loss, variables)

            optimizer.apply_gradients(zip(gradients, variables))

            print('Epoch {} Batch {} Loss {:.4f}'.format(
                epoch + 1, batch, batch_loss.numpy()))

        # saving (checkpoint) the model every 2 epochs
        if (epoch + 1) % 2 == 0:
            checkpoint.save(file_prefix=checkpoint_prefix)

        print('Epoch {} Loss {:.4f}'.format(epoch + 1,
                                            total_loss / N_BATCH))
        print('Time taken for 1 epoch {} sec\n'.format(time.time() - start))


def evaluate(sentence, encoder, decoder, inp_lang, targ_lang, max_length_inp, max_length_targ):
    # attention_plot = np.zeros((max_length_targ, max_length_inp))

    # Extract the INPUT from the sentence given
    sentence = preprocess_sentence(sentence)
    print("Evaluating: {}".format(sentence))

    inputs = [inp_lang.word2idx[i] for i in sentence.split(' ')]
    inputs = tf.keras.preprocessing.sequence.pad_sequences(
        [inputs], maxlen=max_length_inp, padding='post')
    inputs = tf.convert_to_tensor(inputs)

    result = ''

    # Init the hidden layer and send input to ENCODER
    hidden = [tf.zeros((1, units))]
    enc_out, enc_hidden = encoder(inputs, hidden)

    # Prepare the decoder with the encoders hidden layer and a starting input
    dec_hidden = enc_hidden
    dec_input = tf.expand_dims([targ_lang.word2idx['<start>']], 0)

    for t in range(max_length_targ):
        predictions, dec_hidden, attention_weights = decoder(
            dec_input, dec_hidden, enc_out)

        # storing the attention weights to plot later on
        attention_weights = tf.reshape(attention_weights, (-1, ))
        # attention_plot[t] = attention_weights.numpy()

        predicted_id = tf.argmax(predictions[0]).numpy()

        result += targ_lang.idx2word[predicted_id] + ' '

        if targ_lang.idx2word[predicted_id] == '<end>':
            # return result, sentence, attention_plot
            return result, sentence

        # the predicted ID is fed back into the model
        dec_input = tf.expand_dims([predicted_id], 0)

    # return result, sentence, attention_plot
    return result, sentence


def translate(sentence, encoder, decoder, inp_lang, targ_lang, max_length_inp, max_length_targ):
    result, sentence = evaluate(
        sentence, encoder, decoder, inp_lang, targ_lang, max_length_inp, max_length_targ)

    print('Input: {}'.format(sentence))
    print('Predicted translation: {}\n'.format(result))

    # attention_plot = attention_plot[:len(result.split(' ')), :len(sentence.split(' '))]
    # plot_attention(attention_plot, sentence.split(' '), result.split(' '))


# ----------------------------------------------------------------------------------------------------
# MAIN
# ----------------------------------------------------------------------------------------------------
if __name__ == "__main__":
    # ------------------------------
    # LOAD DATASET
    # ------------------------------
    # DATA_DIR = "../data/eng-gloss/"
    # filename = "eng-gloss.txt"
    DATA_DIR = "../data/eng-afr/"
    filename = "eng-afr.txt"
    path_to_file = DATA_DIR + filename
    print("\n...Loading Dataset From {}...\n".format(filename))
    input_tensor, target_tensor, inp_lang, targ_lang, max_length_inp, max_length_targ = load_dataset(
        path_to_file)
    # Print every word in the input language
    # for count,w in enumerate(inp_lang.vocab):
    #     print("{}: {}".format(count,w))

    # ------------------------------
    # SPLIT THE DATASET
    # ------------------------------
    # Creating training and validation sets using an 80-20 split,
    # this method maintains the pairs from the input array to the output array
    print("\n...Splitting the dataset...\n")
    input_tensor_train, input_tensor_val, target_tensor_train, target_tensor_val = train_test_split(
        input_tensor, target_tensor, test_size=0.2)
    print("Dimensions of raw training dataset (input, target): \t{}, {}".format(
        input_tensor_train.shape, target_tensor_train.shape))
    print("Dimensions of validation dataset (input, target): \t{}, {}".format(
        input_tensor_val.shape, target_tensor_val.shape))

    # ------------------------------
    # CREATE TF DATASET
    # ------------------------------

    BUFFER_SIZE = len(input_tensor_train)   # Number of samples in training set
    BATCH_SIZE = 16                         # Batch size for training
    N_BATCH = BUFFER_SIZE//BATCH_SIZE       # Number of batches in training set
    embedding_dim = 256                     # the size of the hidden state
    units = 1024                            # output dimension?

    vocab_inp_size = len(inp_lang.word2idx)     # size input language vocab
    vocab_tar_size = len(targ_lang.word2idx)    # size target language vocab

    print("\n...Creating the TF Training dataset...\n")
    # dataset = tf.data.Dataset.from_tensor_slices(
    #     {
    #         "eng": input_tensor_train,
    #         "gloss": target_tensor_train
    #     }).shuffle(BUFFER_SIZE)
    # dataset = dataset.batch(BATCH_SIZE, drop_remainder=True)
    dataset = tf.data.Dataset.from_tensor_slices(
        (input_tensor_train, target_tensor_train)).shuffle(BUFFER_SIZE)
    dataset = dataset.batch(BATCH_SIZE, drop_remainder=True)
    print(input_tensor_train)
    print(dataset)
    # print("Input Language: \n- Data type: \t\t\t\t{}\n- Output shape of dataset: \t{}\n- Length of input sentences: \t\t{}".format(
        # dataset.output_types, dataset.output_shapes, dataset.output_shapes))

    # ------------------------------
    # Initialise the Model
    # ------------------------------
    encoder = Encoder(vocab_inp_size, embedding_dim, units, BATCH_SIZE)
    decoder = Decoder(vocab_tar_size, embedding_dim, units, BATCH_SIZE)
    optimizer = tf.train.AdamOptimizer()

    checkpoint_dir = '../training_checkpoints'
    checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
    checkpoint = tf.train.Checkpoint(optimizer=optimizer,
                                     encoder=encoder,
                                     decoder=decoder)

    # ------------------------------
    # Check the user input
    # ------------------------------
    try:
        while(True):
            print("\n\nChoose one of the options: ")
            print("\t1. Train \n\t2. Translate")
            choice = input("Enter your choice here: ")
            if(choice == "1"):

                print("\nBeginning the training...")
                train()

            elif(choice == "2"):

                checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))
                sentence = input("\nWhat would you like to translate? ")
                # translate(u'hace mucho frio aqui.', encoder, decoder, inp_lang, targ_lang, max_length_inp, max_length_targ)
                translate(sentence, encoder, decoder, inp_lang,
                          targ_lang, max_length_inp, max_length_targ)

            else:
                print("Invalid choic. Try again...")

    except(KeyboardInterrupt):
        print("Closing...")

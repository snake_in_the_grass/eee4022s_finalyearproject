# ------------------------------------------------------------------------
# Author:       Jake Pencharz
# Description:  Methods and classes used for an encoder-decoder model
# Date:         July 2019
# Project:      Translate English to SASL, UCT Final year project
# 
# ** Based on tensorflow tutorial: 
# https://www.tensorflow.org/beta/tutorials/text/nmt_with_attention
# ------------------------------------------------------------------------



from __future__ import absolute_import, division, print_function
import tensorflow as tf
tf.enable_eager_execution()
import numpy as np

'''
Gated Recurrent Unit
'''
def gru(units):
  # If GPU is detected, use CuDNNGRU(provides a 3x speedup than GRU)
    if tf.test.is_gpu_available():
        print("GPU detected, using DuDNNGRU")
        return tf.keras.layers.CuDNNGRU(
            units,
            return_sequences=True,
            return_state=True,
            recurrent_initializer='glorot_uniform')
    else:
        return tf.keras.layers.GRU(
            # Positive integer, dimensionality of the output space.
            units,
            return_sequences=True,  # returns the whole output sequence
            return_state=True,      # returns the last state in addition to the output
            recurrent_activation='sigmoid',
            # intialises the weights for state transformation matrix, drawing samples from a uniform distribution
            recurrent_initializer='glorot_uniform'
        )


'''
Loss Function
'''
def loss_function(real, pred):
        mask = 1 - np.equal(real, 0)
        # Calculate loss
        loss_ = tf.nn.sparse_softmax_cross_entropy_with_logits(
            labels=real, logits=pred) * mask
        return tf.reduce_mean(loss_)


'''
Encoder
'''
class Encoder(tf.keras.Model):
    def __init__(self, vocab_size, embedding_dim, enc_units, batch_sz):
        super(Encoder, self).__init__()
        self.batch_sz = batch_sz
        self.enc_units = enc_units
        self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim)
        self.gru = gru(self.enc_units)

    def call(self, x, hidden):
        x = self.embedding(x)
        output, state = self.gru(x, initial_state=hidden)
        return output, state

    def initialize_hidden_state(self):
        return tf.zeros((self.batch_sz, self.enc_units))


'''
Decoder
'''
class Decoder(tf.keras.Model):
    def __init__(self, vocab_size, embedding_dim, dec_units, batch_sz):
        super(Decoder, self).__init__()
        self.batch_sz = batch_sz
        self.dec_units = dec_units
        self.embedding = tf.keras.layers.Embedding(vocab_size, embedding_dim)
        self.gru = gru(self.dec_units)
        self.fc = tf.keras.layers.Dense(vocab_size)

        # used for attention
        self.W1 = tf.keras.layers.Dense(self.dec_units)
        self.W2 = tf.keras.layers.Dense(self.dec_units)
        self.V = tf.keras.layers.Dense(1)

    def call(self, x, hidden, enc_output):
        # enc_output shape == (batch_size, max_length, hidden_size)

        # hidden shape == (batch_size, hidden size)
        # hidden_with_time_axis shape == (batch_size, 1, hidden size)
        # we are doing this to perform addition to calculate the score
        hidden_with_time_axis = tf.expand_dims(hidden, 1)

        # score shape == (batch_size, max_length, 1)
        # we get 1 at the last axis because we are applying tanh(FC(EO) + FC(H)) to self.V
        score = self.V(tf.nn.tanh(self.W1(enc_output) +
                                  self.W2(hidden_with_time_axis)))

        # attention_weights shape == (batch_size, max_length, 1)
        attention_weights = tf.nn.softmax(score, axis=1)

        # context_vector shape after sum == (batch_size, hidden_size)
        context_vector = attention_weights * enc_output
        context_vector = tf.reduce_sum(context_vector, axis=1)

        # x shape after passing through embedding == (batch_size, 1, embedding_dim)
        x = self.embedding(x)

        # x shape after concatenation == (batch_size, 1, embedding_dim + hidden_size)
        x = tf.concat([tf.expand_dims(context_vector, 1), x], axis=-1)

        # passing the concatenated vector to the GRU
        output, state = self.gru(x)

        # output shape == (batch_size * 1, hidden_size)
        output = tf.reshape(output, (-1, output.shape[2]))

        # output shape == (batch_size * 1, vocab)
        x = self.fc(output)

        return x, state, attention_weights

    def initialize_hidden_state(self):
        return tf.zeros((self.batch_sz, self.dec_units))

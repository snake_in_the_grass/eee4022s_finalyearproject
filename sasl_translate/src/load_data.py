# ------------------------------------------------------------------------
# Author:       Jake Pencharz
# Description:  Methods and classes used fto load parrelel corpus data
# Date:         July 2019
# Project:      Translate English to SASL, UCT Final year project
# 
# ** Based on tensorflow tutorial: 
# https://www.tensorflow.org/beta/tutorials/text/nmt_with_attention
# ------------------------------------------------------------------------

import unicodedata
import re
import tensorflow as tf
tf.enable_eager_execution()


'''
Converts the unicode string to ascii
'''
def unicode_to_ascii(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
        if unicodedata.category(c) != 'Mn')

'''
Formats a sentence to:
1. remove any special characters 
2. space between words and punctuation
3. add in <start> and <end> tokens
'''
def preprocess_sentence(w):
    w = unicode_to_ascii(w.lower().strip())
    
    # creating a space between a word and the punctuation following it
    w = re.sub(r"([?.!,¿])", r" \1 ", w)
    w = re.sub(r'[" "]+', " ", w)
    
    # replacing everything with space except (a-z, A-Z, ".", "?", "!", ",")
    w = re.sub(r"[^a-zA-Z?.!,¿]+", " ", w)
    w = w.rstrip().strip()
    
    # adding a start and an end token to the sentence
    w = '<start> ' + w + ' <end>'
    return w

'''
Returns sentence pairs in the format: [ENGLISH, GLOSS]
'''
def create_dataset(path):
    lines = open(path, encoding='UTF-8').read().strip().split('\n')
    
    # Split the sentence using four spaces and process/clean each half seperately, combining the result
    sentence_pairs = [[preprocess_sentence(w) for w in l.split('\t')]  for l in lines ]
    return sentence_pairs

'''
This class creates a word -> index mapping (e.g,. "dad" -> 5) and vice-versa 
(e.g., 5 -> "dad") for each language,
'''
class LanguageIndex():
  def __init__(self, lang):
    self.lang = lang
    self.word2idx = {}
    self.idx2word = {}
    self.vocab = set()
    
    self.create_index()
    
  def create_index(self):
    for phrase in self.lang:
      self.vocab.update(phrase.split(' '))
    
    self.vocab = sorted(self.vocab)
    
    # Populate the word to index dictionary
    self.word2idx['<pad>'] = 0
    for index, word in enumerate(self.vocab):
      self.word2idx[word] = index + 1
    
    # Populate the index to word dictionary
    for word, index in self.word2idx.items():
      self.idx2word[index] = word

def max_length(tensor):
    return max(len(t) for t in tensor)


def load_dataset(path):
    # creating cleaned input, output pairs
    pairs = create_dataset(path)

    # index language using the class defined above
    # consider only using one language class since glossing uses english words
    inp_lang = LanguageIndex(en for gloss, en in pairs)
    targ_lang = LanguageIndex(gloss for gloss, en in pairs)
    
    # Vectorize the input and target languages (ONE HOT ENCODING OF SENTENCES)
    
    # English sentences
    input_tensor = [[inp_lang.word2idx[s] for s in en.split(' ')] for gloss, en in pairs]
    
    # Glossed SASL sentences
    target_tensor = [[targ_lang.word2idx[s] for s in gloss.split(' ')] for gloss, en in pairs]
    
    # Calculate max_length of input and output tensor
    # Here, we'll set those to the longest sentence in the dataset
    max_length_inp, max_length_tar = max_length(input_tensor), max_length(target_tensor)

    print("Maximum length of input sentence: {}".format(max_length_inp))
    print("Maximum length of target sentence: {}".format(max_length_tar))
    
    # Padding the input and output tensor to the maximum length
    input_tensor = tf.keras.preprocessing.sequence.pad_sequences(input_tensor, 
                                                                 maxlen=max_length_inp,
                                                                 padding='post')
    
    target_tensor = tf.keras.preprocessing.sequence.pad_sequences(target_tensor, 
                                                                  maxlen=max_length_tar, 
                                                                  padding='post')
    
    return input_tensor, target_tensor, inp_lang, targ_lang, max_length_inp, max_length_tar      

from progress.bar import Bar
import operator


def map_book(tokens):
    hash_map = {}   # create a dictionary
    print("Parsing through {} words...".format(len(tokens)))

    if tokens is not None:

        for element in tokens:
            # Remove Punctuation
            word = element.replace(",", "")
            word = word.replace(".", "")
            word = word.replace("?", "")

            # Word Exist?
            if word in hash_map:
                hash_map[word] = hash_map[word] + 1
            else:
                hash_map[word] = 1
        return hash_map
    else:
        print("No tokens")
        return None


def analyse_text_file(data_dir, filename):
    # Open the file
    with open(data_dir+filename, 'r') as file:

        if(file is None):
            print("Error opening the file")
            return

        text = file.read()              # String of entire content
        words = text.lower().split()    # Tokenise the string
        map = map_book(words)           # Put words into a dictionary
        sorted_map = sorted(map.items(), key=operator.itemgetter(
            1), reverse=True)           # sort the map
        
        print("Number of unique words in {}: {}".format(filename, len(sorted_map)))
        sample_size = 20
        print("The {} most common words in {} are:".format(sample_size, filename))
        for freq_pair in sorted_map[0:sample_size]:
            print(freq_pair)        


def main():
    text_files = []
    wiki_dump = ("/Users/jakepencharz/Documents/UCT-2019/EEE4022-FinalProject/sasl_translate/data/Simple_Wikipedia_Dump/", "wiki_dump.txt")
    text_files.append(wiki_dump)
    bnc_fiction = ("/Users/jakepencharz/Documents/UCT-2019/EEE4022-FinalProject/sasl_translate/data/BritishNationalCorpus_XML/", "fiction.txt")
    text_files.append(bnc_fiction)
    bnc_news = ("/Users/jakepencharz/Documents/UCT-2019/EEE4022-FinalProject/sasl_translate/data/BritishNationalCorpus_XML/", "news.txt")
    text_files.append(bnc_news)
    bnc_magazine = ("/Users/jakepencharz/Documents/UCT-2019/EEE4022-FinalProject/sasl_translate/data/BritishNationalCorpus_XML/", "aca.txt")
    text_files.append(bnc_magazine)
    

    for file in text_files:
        print("------------------------------------------------")
        print("Analysing {}".format(file[1]))
        analyse_text_file(file[0], file[1])

    
        
        


if __name__ == "__main__":
    main()

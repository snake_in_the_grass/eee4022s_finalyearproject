
import numpy as np
import pandas as pd

path_to_text_file = "/Users/jakepencharz/Documents/UCT-2019/EEE4022-FinalProject/sasl_translate/data/eng-afr/eng-afr.txt"
lines = open(path_to_text_file, encoding='UTF-8').read().strip().split('\n')  

# sample size (try with smaller sample size to reduce computation)
num_examples = 30000 

# creates lists containing each pair
original_sentence_pairs = [[w for w in l.split('\t')] for l in lines[:num_examples]]

data = pd.DataFrame(original_sentence_pairs, columns=["af", "en"])

# Switch the columns
columnsTitles=["en","af"]
data=data.reindex(columns=columnsTitles)

print(data)

# Filter the sentence sizes 
data = data[~(data.af.str.len() > 100)]

print(data)

# Save the cleaned data
data["en"] = data["en"] + "\t"
np.savetxt("./test.txt", data.values, fmt='%s')
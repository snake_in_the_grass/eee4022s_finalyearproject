{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Recurrent Neural Networks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is intuitive for humans to understand time dependant _sequences_ of data by considering the context of the data. Understanding a sentence does not come from the word you are currently reading, but by considering it in the context of all the words that have come previously. \n",
    "\n",
    "A traditional neural network (NN) cannot do this. But RNNs use a memory mechanism to capture information over the duration of a sequence. If one unrolls the RNN the chain like nature of the arcitechture reveals how ideal it is for sequenced data.\n",
    "\n",
    "<img src=\"./Figures/RNN-rolled.png\" alt=\"Rolled RNN\" style=\"width: 100px;\"/>\n",
    "\n",
    "This feedback allows one to:\n",
    "- Track long term depencies\n",
    "- Handle variable length input sequences (a fixed window moving over a sequence would not capture adequete context)\n",
    "- Maintain informatio about the order of tokens in a sequnce (as opposed to a bag of words representation which can model the entire sequence but looses all the information about order)\n",
    "- Share parameters/weights across the entire sequence (so that a word occuring at any point in the sequence still sees the same parameters which makes it more robust to altered word ordering)\n",
    "\n",
    "Some usage categories are:\n",
    "- __Many to One:__ some text to a sentiment (text classification)\n",
    "- __Many to Many:__ sequence to sequence transformation/translation\n",
    "\n",
    "The RNN uses a hidden state as its memory. This hidden state is updated every time step (for every item in the sequence). The value of the updated state depends on the current input, and the previous state. A general state update equation is:\n",
    "\n",
    "$$\n",
    "h_t = f_W(h_{t-1}, x_t)\n",
    "$$\n",
    "\n",
    "Here $f_W$ is a function which is parameterised by a set of learned weights $W$, which acts on the previous state and the current input to update the RNN cell's state."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Vanilla RNN\n",
    "\n",
    "The simplest RNN architechture uses a tanh function to update its state. And another weight matrix to calculate the output (if required) from the current state. \n",
    "\n",
    "\\begin{align}\n",
    "    h_t &= tanh(W_{hh}h_{t-1} + W_{xh}x_t) \\\\\n",
    "    y &= W_{hy}h_t\n",
    "\\end{align}\n",
    "\n",
    "Below is a figure which shows the RNN 'unrolled' with time, but lacking an output. The $tanh$ function is used to update the state ever time step. The final state in the sequence __summarises__ the sequence. Importantly, the same weights are used at every time step in the sequence. \n",
    "\n",
    "<img src=\"./Figures/LSTM3-SimpleRNN.png\" alt=\"Unrolled Vanilla RNN\" style=\"width: 600px;\"/>\n",
    "\n",
    "During training, each sequence is considered to be a batch, and the weights are only updated once the entire sequenced has been processed. If there were an output, a loss could be calculated at each time step by comparing the expected output with the token that the RNN produced. The total loss can then be found by summing up all of these losses to perform back propogation. Back propogation is performed 'through time' in these networks looking back over the entire sequence.\n",
    "\n",
    "__The problem:__ However, during back propogation since there is a high potential of either exploding or vanishing gradients. Therefore the LSTM was introduced to improve gradient flow.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Long Short Term Memory\n",
    "\n",
    "LSTMs remove the problem of exploding/vanishing gradients as well as allowing RNNs to learn long term dependancies in practice. They were introduced in 1997 by [Hochreter and Schmidhuber](http://www.bioinf.jku.at/publications/older/2604.pdf). \n",
    "\n",
    "Instead of having a single neural network layer (like in the vanilla RNN), LSTMs have four layers which all serve a purpose. \n",
    "\n",
    "<img src=\"./Figures/LSTM3-chain.png\" alt=\"Unrolled Vanilla RNN\" style=\"width: 600px;\"/>\n",
    "\n",
    "The key addition is the _cell state_ which is never revealed to the outside world and is used to update the cells hidden state. The cell state runs through each iteration with only minor changes. The LSTM uses gates (a sigmoid layer and pointwise multiplication) to carefully adjust the cell state by adding and removing information. The sigmoid layer outputs numbers between 0 and 1. The point wise multiplication (by a number between 0 and 1) acts as a gate allowing information to either pass or be blocked. \n",
    "\n",
    "Updating the cell and hidden state follows the following process using 3 gates:\n",
    "1. __Forget gate__: this uses the current input and the previous state to determine what information in the cell state should persist and what should be forgotten. Using this information, it outputs a number between 0 and 1 for _every_ number in the cell state. A 1 would imply that the information is still relevant and should be kept, while a 0 would mean that the information is no longer important and must be thrown out. One would hope that if the input provides information that overrides that which is currently summarised by the cell state, that the old information would be disgarded (note that this new information would not be added by this gate). The equation for the output from the forget gate is:\n",
    "$$ f_t = \\sigma(W_f[H_{t-1}, x_t] + b_f)$$\n",
    "\n",
    "\n",
    "2. __Input gate:__ in order to update the state, by adding new information, two steps are required. First, the candidate values that could be added to the cell state are created by a $tanh$ layer (squashing all values to between -1 and 1). Then the input gate/sigmoid layer (once again) uses the previous state and the current input to decide which of these candidates should be added. Therefore the gate is not being applied to the current cell state, but to a set of potential additions to the cell state. The equation for the gate is: \n",
    "$$ i_t = \\sigma(W_f[H_{t-1}, x_t] + b_i)$$ \n",
    "And the equation for the candidate additions to the state is: \n",
    "$$ \\tilde{C}_t = \\tanh(W_c[H_{t-1}, x_c] + b_c)$$  \n",
    "\n",
    "3. Now, to update the cell state from $C_{t-1} \\to C_{t}$, one uses the forget and update steps:\n",
    "    - $f_t$ is multiplied pointwise by $C_{t-1}$ to forget all irrelevant old information\n",
    "    - $i_t$ is multiplied pointwise by $\\tilde{C}_t$ and then added to $C_{t-1}$\n",
    "    - Therefore, the full update equation for the cell state is: (note that $\\circ$ refers to an element wise multiplication)\n",
    "    $$ C_t = f_t \\circ C_{t-1} + i_t \\circ \\tilde{C}_t$$\n",
    "\n",
    "4. __Output gate:__ Finally, the hidden state is calculated as a filtered version of the cell state. \n",
    "    - First one pushes the updated cell state through a $tanh$ layer (all values are between -1 and 1).\n",
    "    - Then another gate/sigmoid layer is used as a filter to determine what information will be passed on from the cell state to the hidden state. \n",
    "    \\begin{align}\n",
    "        &o_t = \\sigma(W_o[H_{t-1}, x_t] + b_o)\\\\\n",
    "        &h_t = o_t \\circ tanh(C_t)\n",
    "    \\end{align}\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Language Translation\n",
    "\n",
    "RNNs are ideal for learning patterns in sequences, and since human languages can be modelled as complex patterns, they are an ideal candidate to model them. \n",
    "\n",
    "The translation method is based on an encoder-decoder architechture. A sentence in English is encoded by an RNN into a vector of numbers which summarises the sentence. This is the final hidden state of the RNN. This hidden state vector is then used as the starting hidden state vector for a second RNN which decodes the summarised information into another language. The decoder outputs words one at a time, feedaing each of its outputs into the next stages input. This is termed sequence to sequence translation, and works for any type of sequence. There is no need for the input and output sequences to be of the same length. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Attention\n",
    "\n",
    "[Article about Attention](http://www.wildml.com/2016/01/attention-and-memory-in-deep-learning-and-nlp/)\n",
    "\n",
    "The entire information contained in the input sentence is essentialy squashed into the sentence embedding by the encoder. It is almost unreasonable to expect all of that information to be well maintained and for the decoder to be able to extract it. \n",
    "\n",
    "The solution is to stop trying to encode _all_ of the information in the final hidden state fixed length vector. The decoder is allowed to 'attend' to different parts of the source sentence at each steo of output generation. The model will learn to focus on the parts of the input sequence that are most important to the output it is currently generating. \n",
    "\n",
    "The output words now depends on a weighted combination of all the encoder's states (not only the last state). When determining an output word, the decoder takes the input state from the previous time step and concatenates a weighted sum of all the ecoders states. The weights for the ecoder states should sum to one (to be a probablity distribution over the input sequences states). \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}

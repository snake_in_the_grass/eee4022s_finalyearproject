\documentclass[11pt, twoside, a4paper]{article}

\usepackage{my_basic_report}
\usepackage{bm}			% Making letters in math mode bold (and not italicised)
\usepackage{subcaption}
	\graphicspath{ {./Figures/}, {/Users/jakepencharz/Library/TeXshop/Logos/}} 		% Set the graphics path 
\usepackage{datetime}
	\newdateformat{monthyeardate}{%
  		\monthname[\THEMONTH], \THEYEAR}

\usepackage{booktabs}			% allows top rule and midrule
\usepackage{diagbox}
\usepackage[export]{adjustbox}		%allows graphic alignment using includegrahics https://tex.stackexchange.com/questions/91566/syntax-similar-to-centering-for-right-and-left
\usepackage{hyperref,wasysym}

%\usepackage[final]{pdfpages}

\lstset{style=Matlab}


%--------------------------------------------------------------------------------------------------------------------------------------------------
% Set Project Information
%---------------------------------------------------------------------------------------------------------------------------------------------------

\title{Translating English Text to South African Sign Language\\[0.5em](Ethics Clearance Application)}  
\author{Jake Pencharz}                                                               
\def\supervisor{Dr M.S Tsoeu}
\def\studentnumber{PNCJAK001}
\def\coursename{Control Engineering}
\def\coursecode{EEE4020F}
\def\university{University Of Cape Town}
\date{\monthyeardate \today}                                                

\makeatletter
\let\thetitle\@title
\let\theauthor\@author
\let\thedate\@date
\makeatother



% ====================================================================================
% BEGIN DOCUMENT
% ====================================================================================
\begin{document}
\raggedbottom

%--------------------------------------------------------------------------------------------------------------------------------------------------
% Title Section
%---------------------------------------------------------------------------------------------------------------------------------------------------
\begin{titlepage}
    \centering
\vspace*{0.5 cm}
\includegraphics[scale = 0.5]{uct_logo}\\[1.0 cm]        
\textsc{\LARGE \university}\\[2.0 cm]   
    \textsc{\Large \coursecode}\\[0.5 cm]                            
%    \textsc{\large \coursename}\\[0.5 cm]                                
    \rule{\linewidth}{0.2 mm} \\[0.4 cm]
    { \huge \bfseries \thetitle }\\
    \rule{\linewidth}{0.2 mm} \\[1.5 cm]

    	\begin{minipage}{0.4\textwidth}
            	\begin{flushleft} \large
                    	\emph{Researcher:}\\
			\theauthor \\
             	\end{flushleft}
   	\end{minipage}~
	\begin{minipage}{0.4\textwidth}
                	\begin{flushright} \large
                    	\emph{Student Number:} \\
                    	\studentnumber                                                                       
           	\end{flushright}
    	\end{minipage}\\[0.5 cm]
	
	\begin{minipage}{0.4\textwidth}
            	\begin{flushleft} \large
                    	\emph{Supervisor:}\\
			\supervisor \\
             	\end{flushleft}
   	\end{minipage}~
	\begin{minipage}{0.4\textwidth}
                	\begin{flushright} \large
                    	                                                     
           	\end{flushright}
    	\end{minipage}\\[5 cm]
	
    {\large \thedate}\\[2 cm]

    \vfill
\thispagestyle{empty}
\end{titlepage}
%===================================================================================


%===================================================================================
\tableofcontents
\thispagestyle{empty}
\newpage

\includepdf[pages=-]{ethics_application_form.pdf}



%--------------------------------------------------------------------------------------------------------------------------------------------------
% INTRODUCTION
%--------------------------------------------------------------------------------------------------------------------------------------------------

\setcounter{page}{1}
\section{Outline of Proposed Research}
\subsection{Summary of the problem statement}
There is a language barrier between the deaf and hearing communities in South Africa that must be narrowed to afford equal rights to those who are hard of hearing. In South Africa, many in the deaf community use South African Sign Language (SASL) to communicate. This language is unique to South Africa and forms a major part of the community's culture and identity. There is currently no standardised dialect for SASL, with variations naturally arising anywhere that there is a large deaf community. 

The language barrier exists because of society's lack of understanding, and an underdeveloped education system for the deaf and hard of hearing. In South Africa many deaf children enter the education system with no formal language skills. In most cases they would not have learnt SASL from their parents, and they would have no concept of spoken languages before this point. It is therefore no surprise that students struggle learning to read and write in English. Students are expected to learn both English and SASL simultaneously. Even in the case where a child is fluent in sign language, learning to read a spoken language like English is challenging. This is because sign languages are not dependant on spoken languages for their structure or vocabulary. It is not surprising that learning to read a language that one does not understand is a challenging task. Furthermore, many studies indicate that by the age of seven (when a child is taught to read), students may have passed the optimum age to become fully proficient in any language.

Therefore, contrary to what most hearing people assume, many members of the deaf community in South Africa are not literate in English (or other native languages) even if they are fluent in SASL.  In order for the two groups to communicate more effectively they would require a bidirectional translation tool. 

\subsection{Research Aims}
This research aims to begin bridging the language gap between hearing and deaf citizens of South Africa. The focus is on translating English to SASL, allowing SASL users to communicate using a language which is most natural for them. 

Therefore the goal of the project is to develop a tool that can translate English text to SASL.

\section{Outline of Research Methodology}
The current state of the art in language translation using machine learning techniques. The field of research relating encompassing these techniques is broadly referred to as Neural Machine Translation (NMT). Many of these algorithms require a vast amount of data to achieve useful levels of translation accuracy. 

Therefore, the research task will require collecting video data of SASL users to train the NMT models. The task will be broken down into three phases:

\begin{enumerate}[label=\textbf{Stage \arabic*}, align=left]
\item
Train a model to convert an English to SASL gloss (a written form of SASL)
\item
Train a model to use the glossed output sentence from Stage 1 to generate a numeric representation (time series) of an appropriate set of signs
\item
Finally using the time series generated in Stage 2, animate the an avatar signing the original sentence in SASL
\end{enumerate}

The first two steps require data to be collected to train machine learning algorithms. Useful data will be in video form so that the movements of a SASL users can be extracted using image processing techniques. Therefore the research will require a large data base of video footage featuring SASL users signing naturally and facing the camera. Since a lot of information is conveyed using non-manual features (facial expression, and positioning of the hands in space around the body), the subjects of these videos will be filmed from the torso upwards, and their faces will be included. Data will be collected from public or private third party sources, or by filming SASL users in a controlled environment.

More specifically, the data is used in the following ways: for Stage 1, the content of the videos will be translated from SASL to gloss and English. This derived information will be used to train a translation algorithm. For Stage 2, the sign language user's movements in the video footage will be tracked and aligned to the English translation as well as the gloss transcription. This will be used to train a second algorithm which will generate appropriate motion time sequences given a certain glossed input. These motion time sequences will be fed to the animation engine.


\section{Data Collection}
Data will either be generated by the researcher in a controlled environment, or acquired from a third party. Any data that is collected during research will not be shared or distributed without the explicit consent of the university. It should be noted that the data may be stored for an extended period of time since the project may run over several years.

\subsection{Collection from a third party}
In the case that the data is acquired, the third party supplying the data will remain anonymous (unless is is requested that they are credited explicitly). Furthermore the sign language user will not be named, and no personal details will be shared.


\subsection{Self-generated Data}
If the data is rather created by the researcher, the subjects will be required to give consent before participating in the study. All potential subjects will be aware of the aims of the study, as well as what their video footage will be used for. No unnecessary personal details will be published concerning any of the study participants. 

The participant will also be given an option as to whether or not they are happy to share their raw data.

\section{Privacy and Anonymity}
The research will strive to maintain the highest level of anonymity as is possible without comprising the data. Anonymity in this case means removing identifying personal information such as names, ID numbers, etc. from the data stores. Personal information of sign language users who participate will not be shared. Personal details will include the subjects age, sex, gender, languages in which they are fluent, and whether or not they are indeed deaf or hard of hearing. 





\section{Informed Consent}
The participants will be given informed consent forms with the aims and objectives of the study. It will be explained to them the duration of the research and how long they will be involved in the research.

The informed consent letters that each participant will receive, will clearly state that they are free to discontinue from the research, should they feel uncomfortable to continue to be part of the controlled environment that recordings are done in.


% ---------------------------------------------------------------------------------------------------------------------------------------------------
% APPENDIX
% ---------------------------------------------------------------------------------------------------------------------------------------------------
\newpage

%\section*{APPENDIX}
\setcounter{page}{1}
\renewcommand{\thepage}{\roman{page}}
\appendix
\setcounter{figure}{0} 	%figures start counting from zero again
\renewcommand\thefigure{\thesection.\arabic{figure}} 	% makes the numbers appear as Figure A.1:
\setcounter{table}{0} 		%tables start counting from zero again
\renewcommand\thetable{\thesection.\arabic{table}} 


% ---------------------------------------------------------------------------------------------------------------------------------------------------
%  SEC: 
% --------------------------------------------------------------------------------------------------------------------------------------------------- 
\section{Letter to Research Participants}
\begin{figure}[H]
	\includegraphics[scale = 0.5,right]{uct_logo}\\[1.0 cm]        
\end{figure}

Dear Research Participant,\\[1em]

My name is Jake Pencharz and I am currently in my fourth year of my undergraduate degree in Electrical and Computer Engineering. I am conducting research for my final year project, and I would deeply appreciate your participation.

My research involves translating English to South African Sign Language (SASL). The final goal of this research is a tool or application that is user friendly and can accept an English sentence as input and then generate an animated representation of the sentence in SASL. Although this tool will not be the outcome of this project, it will be an important step towards the final goal. 

Translating between these vastly different languages is a challenge. As an engineering student I am attempting to solve this problem using Machine Learning tools and techniques. This type of research requires the collection of data in order to train the machine learning models. 

\textbf{Why the research requires you:}\\
You have been approached to participate based on your proficiency in SASL. 

\textbf{What type of data is collected:}\\
Video data is being collected since sign languages are inherently visual. Therefore, as a participant, you would be agreeing to be filmed from the waist upwards whilst using SASL. The duration of content will be around 1 hour per session. This video footage will be owned by the University of Cape Town.

\textbf{Potential Social Harm:}\\
If the raw data is made public, there is the potential for social backlash. The participant should be aware that their signing may be under the scrutiny of the public. Once again, if the participant is not willing to put themselves in the public eye, they can specify this at the bottom of this form. 

\textbf{Data Security:}\\
It is intended that the raw data is publicly accessible for research purposes. However the participant may indicate (at the bottom of this form) that they would not like their raw data to be shared. If data is shared with the research community, it will be shared responsibly without revealing any personal details about the participants.  All collected data will be stored and protected in perpetuity by UCT. 

\textbf{Freedom of participation:}\\
Any participation in this study is voluntary. There will be no consequences if you choose not to participate in the study. At any time during the process of collecting data you may withdraw without incurring any negative consequences. 

\textbf{Benefits of participation:}\\
There are many sign language users in South Africa who have limited literacy in English. This tool would allow English speakers to communicate more effectively with those dependant on SASL. This is to the benefit of the hearing community as well as allowing the deaf community easier access to information. 


\textbf{Confidentiality and Anonymity:}\\
It may be possible to blur or otherwise obfuscate defining facial features of the participants in the recordings in an attempt to keep them anonymous. This is optional and should be requested by the participant. Blurring of any facial features may make the research substantially more difficult since signed languages rely on facial expressions for communication.

Other personal details of the participants will not be shared. No names will be recorded, rather pseudonyms will be used. There will be no metadata associated with the video recordings that could be used to identify or track the individuals in the study. This means that participating in this study will not make one vulnerable to personal attacks, or any sort of character defamation. 

If the research is published in any form, a participant may request that their name be included on the publication. This name will still not be directly linked to any of the data that they contributed. 

\vfill

\begin{tabular}{@{}p{.8in}p{4in}@{}}
Signed: & \hrulefill \\
& Mr Jake Pencharz (Researcher) \\
& BSc Electrical \& Computer Engineering 
\end{tabular}

\begin{tabular}{@{}p{.8in}p{4in}@{}}
Approved: & \hrulefill \\
& Dr Mohohlo Tsoeu (Supervisor)\\
& MSc(Eng) PhD, Cape Town 
\end{tabular}



% ---------------------------------------------------------------------------------------------------------------------------------------------------
%  SEC: 
% --------------------------------------------------------------------------------------------------------------------------------------------------- 
\newpage
\section{Consent Form}
\begin{figure}[H]
	\includegraphics[scale = 0.5,right]{uct_logo}\\[1.0 cm]        
\end{figure}


Dear Research Participant,\\[1em]

Kindly take note that this consent form will be kept strictly confidential and not associated with the data collected. If you are willing to participate in the study kindly give consent below by selecting whichever is applicable:


I  \; \rule{20em}{0.07em} \; agree to participate in the study and fully understand the extent of my participation. All of my questions about my participation have been answered. I understand that I am free to withdraw my participation at any time without any prejudice. I confirm that the information provided in this form is true and accurate.

\begin{Form} 
\textit{Please fill in the following check boxes using either a :} {\huge \CheckedBox} \textbf{(yes)}, \textit{or a} {\huge \XBox} \textbf{(no)}:\\

\begin{enumerate}
\item
\CheckBox{I am over the age of 18 years: }
\item
\CheckBox{I have understood the content of the letter above: }
\item
\CheckBox{I understand the data collection process: }
\item
\CheckBox{I understand understand the ramifications of recording the data: }
\item
\CheckBox{I give permission to UCT to share my raw data publicly: }
\item
\CheckBox{I give permission to UCT to share derivates of the raw data only: }
\end{enumerate}

\end{Form} 

\vfill

\begin{tabular}{@{}p{.8in}p{4in}@{}}
Signed: & \hrulefill \\
& Participant
\end{tabular}







%\newpage
%\section*{References}
%
%\begingroup
%\renewcommand{\section}[2]{}
%\footnotesize{
%	\bibliographystyle{IEEEtran}
%	\bibliography{lib.bib}
%}
%\endgroup




% ---------------------------------------------------------------------------------------------------------------------------------------------------
% END DOCUMENT
% ---------------------------------------------------------------------------------------------------------------------------------------------------
\end{document}
% ---------------------------------------------------------------------------------------------------------------------------------------------------